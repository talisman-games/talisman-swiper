# talisman-swiper

This is a fork of [Swiper.js](https://github.com/nolimits4web/swiper)@4.1.6 intended to hack around some specific problems
relating to Swipers placed in HTML elements that have been rotated. It is
not intended for public re-distribution, nor merging back into Swiper.js.

My goal with this use case is the ensure that each rotated Swiper behaves the same from that rotated element's point of view. If the user looks at the rotated element such that it appears right-side-up to them, the Swiper should behave in the same way (ie. a right-to-left swipe gesture advances through the slides).

## Issue Summary

A number of problems occur if you rotate a Swiper (or its parent) using `transform: rotate()`.
Most of these problems can be worked around in the client app. One cannot.

### Swipe Gesture Direction

If you need to rotate a Swiper in each 90-degree rotation increment, you can
kludge it by combining its `direction: 'horizontal'` and
`direction: 'vertical'` properties with forward and reverse slide ordering.

#### Reverse Slide Ordering

Your Swiper would normally contain slides **A**, **B**, **C**. If you know you want to use a swipe gesture direction that requires reverse slide ordering, you should instead create your slides in the order: **C**, **B**, **A**, and then set `initialSlide: 2`.

This starts you at the "last" slide in the sequence, which is the first one you want your user to see, and they need to swiped in the opposite direction to "advance".

For `direction: 'horizontal'` the normal gesture direction is left-to-right (assuming no `rtl` modifier). The opposite direction is right-to-left.

For `direction: 'vertical'` the normal gesture direction is down-to-up. The opposite direction is up-to-down.

NOTE: All of these gestures are in **screen-space coordinates**, which is source of all of the issues discussed on this page. If they were in **element-space coordinates** everything would be fine and dandy.

#### Examples:

1. Swipe **Left** to advance: This is the default behavior. You don't need to do anything (`direction: 'horizontal'` is the default).
1. Swipe **Right** to advance: `direction: 'horizontal'` and reverse the order of the slides.
1. Swipe **Up** to advance: `direction: 'vertical'`.
1. Swipe **Down** to advance: `direction: 'vertical'` and reverse the order of the slides.

### Pagination Bullets

There are two problems with pagination bullets in rotated Swipers:

1. Positioning
1. Numbering

#### Positioning

Swiper defines the positioning of the pagination bullets as: horizontally-centered along the bottom of a horizontal Swiper, and vertically-centered along the right side of a vertical Swiper. That works great, unless the only reason you're changing the horizontal/vertical direction of the swiper is to get the swipe gesture direction you need.

Thankfully, some creative CSS can handle putting the pagination bullets back into the location you want, regardless of the Swiper direction. In my case, this was always horizontally-centered along the bottom of the Swiper (in rotated element-space).

#### Numbering

Astute readers will realize that following the advice in the **Reverse Slide Ordering** section above will cause numbering problems for any pagination controls.

In the example used in that section, the pagination will begin at 3/3 (third bullet active) when the slide order is reversed.

In my case, I needed the bullet style of pagination. To put the bullets back in the expected visual order, a 180-degree rotation of the pagination element when using reverse slide ordering was the chosen work-around. This would not work for the other pagination styles.

### Transition Effects

All slide transition effects are accomplished via CSS `transform` properties. That means they resepect the rotation of the Swiper element.

_However_, the JavaScript code that defines the coordinate space for each `transform` assumes screen-space coordinates for that transformation. That means if the Swiper is rotated, the transformation will no longer match the swipe gesture direction. This causes a jarring disconnect between the gesture and the animation.

Unfortunately as this screen-space assumption is baked into the JavaScript and it cannot be untangled via CSS, this has to be changed in the Swiper code itself. Hence this repository.

## The Correct Solution

Obviously, the right way to solve all of these problems is to update Swiper.js to use element coordinate-space instead of screen coordinates.

As I started my use of Swiper, that appeared to be a larger and more intimidating problem than simply working around any potential issues.

After solving the swipe gesture direction, slide order reversal, and pagination bullet problems, it seemed that perhaps fixing Swiper directly would have been a better choice. However by this point I was already committed.

Once my deadlines have passed, it would be wise to at least log a GitHub issue to have the author make the changes, if not dig in and attempt the changes myself. Then I would be able to undo a week+ of work-arounds and hacks to get this use case to work as desired.

### Getting Rid of the Work Arounds

In order to fix the transition effects, I had to add a few options to how Swiper handles its touch input processing. I added the following optional parameters:

* `talismanFlipX` (boolean, default: false) - If true, negates X-axis diffs on every touchMove.
* `talismanFlipY` (boolean, default: false) - If true, negates Y-axis diffs on every touchMove.
* `talismanSwapXY` (boolean, default: false) - If true, swaps the X- and Y- axis diffs on every touchMove.

With these combinations of parameters, the calling code can compensate for any 90-degree rotation. **Doing so renders all of the previous work-arounds unnecessary.**

These parameters are, of course, still hacks.

-- Andre Arsenault, 22 Feb 2018
